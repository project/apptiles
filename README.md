# Application Tiles

The module allows you to put the icons to predefined folders of an active theme and see them added as meta tags.

## Usage

Every theme can have its own set of tiles. Place icons, names of which match the pattern `^\d+x\d+\.png$` (i.e. `32x32.png`, `48x48.png`, `128x128.png` etc.), in the following directories:

- `themes/stark/tiles/android`
- `themes/stark/tiles/windows`
- `themes/stark/tiles/ios`

It's up to you to decide what sizes needed. Simply add the files and the job is done.

### Additional configuration (Windows only)

The additional configuration happens in `*.info.yml` of a theme. The following parameters available:

```yml
apptiles:
  msapplication:
    tile:
      TileColor: "#444"
    notification:
      cycle: 1
      frequency: 30
      polling-uri:
        src: /rss.xml
      polling-uri1:
        src: /path/to/rss.xml
```

All these settings are not mandatory and can be omitted.
