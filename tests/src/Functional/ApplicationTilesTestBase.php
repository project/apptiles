<?php

namespace Drupal\Tests\apptiles\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Base abstraction for testing the `Application Tiles` module.
 */
abstract class ApplicationTilesTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['apptiles'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'apptiles_test_theme';

  /**
   * {@inheritdoc}
   *
   * Needed to not validate `system.theme.global` schema.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Check meta tags on page.
   *
   * @param bool $exists
   *   Should the metatags be on the page?
   */
  protected function checkMetatags(bool $exists = TRUE): void {
    $tiles = \Drupal::service('apptiles')->getUrls();
    $urlGenerator = \Drupal::service('file_url_generator');
    // Cannot continue without files.
    $this->assertNotEmpty($tiles, 'Tiles were found!');

    // No need to check tiles for Windows because they
    // are configured inside the `browserconfig.xml`.
    unset($tiles['windows']);

    $html = $this
      ->getSession()
      ->getPage()
      ->getHtml();

    // Walk through existing tiles only.
    foreach ($tiles as $os => $dimensions) {
      foreach ($dimensions as $dimension => $url) {
        $this->assertSame(
          $exists,
          \str_contains($html, $urlGenerator->transformRelative($url)),
          \sprintf('Metatag for "%s" with "%s" dimension%sexists on the page.', $os, $dimension, $exists ? ' ' : ' doses not ')
        );
      }
    }
  }

  /**
   * Recursive comparison of values of tails setting.
   *
   * @param array $settings
   *   An associative array, full representation of XML structure (nested).
   * @param \SimpleXMLElement $element
   *   Complete XML file with settings.
   */
  protected function recursiveSettingsAssertion(array $settings, \SimpleXMLElement $element): void {
    foreach ($settings as $key => $value) {
      if (\is_array($value)) {
        // R-r-recursive.
        \call_user_func(__METHOD__, $settings[$key], $element->{$key});
      }
      // WARNING: do not use `assertEquals()` since it'll be trying to
      // serialize the `SimpleXMLElement` and result in uncaught exception:
      // Serialization of 'SimpleXMLElement' is not allowed.
      elseif (isset($element->{$key})) {
        // Assert child properties.
        $this->assertTrue($settings[$key] == $element->{$key}, \sprintf('Value of "%s" is "%s".', $key, $settings[$key]));
      }
      else {
        // Assert attributes.
        $attribute = $element->attributes()->{$key};
        $this->assertTrue($attribute == $value, \sprintf('Attribute "%s" is "%s".', $attribute, $value));
      }
    }
  }

}
