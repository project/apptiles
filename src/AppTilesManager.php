<?php

namespace Drupal\apptiles;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\Exception\NotRegularDirectoryException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;

/**
 * Implementation of the default application tiles manager.
 */
class AppTilesManager implements AppTilesManagerInterface {

  /**
   * A path to the default theme.
   *
   * @var string
   */
  protected readonly string $themePath;

  /**
   * A set of settings.
   *
   * @var array
   */
  protected readonly array $settings;

  /**
   * Constructs the service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   An instance of the `config.factory` service.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   An instance of the `theme_handler` service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   An instance of the `cache.default` service.
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   An instance of the `router.admin_context` service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   An instance of the `file_system` service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ThemeHandlerInterface $themeHandler,
    protected readonly CacheBackendInterface $cacheBackend,
    protected readonly AdminContext $adminContext,
    protected readonly FileSystemInterface $fileSystem,
  ) {
    $theme = $themeHandler->getTheme($themeHandler->getDefault());

    // Use global config because `theme_get_setting()` could possibly
    // override it. Execution of the next construction will break the
    // result:
    // @code
    // \Drupal::configFactory()
    //   ->getEditable('stark.settings')
    //   ->set(APPTILES_MODULE_NAME, [])
    //   ->save(TRUE);
    // @endcode
    $settings = (array) $configFactory
      ->get('system.theme.global')
      ->get(APPTILES_MODULE_NAME);

    if (!empty($theme->info[APPTILES_MODULE_NAME]) && \is_array($theme->info[APPTILES_MODULE_NAME])) {
      $this->settings = \array_merge($settings, $theme->info[APPTILES_MODULE_NAME]);
    } else {
      $this->settings = $settings;
    }

    $this->themePath = $theme->getPath();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(): array {
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting(string $setting, mixed $default_value = NULL): mixed {
    return $this->settings[$setting] ?? $default_value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath(): string {
    return "$this->themePath/tiles";
  }

  /**
   * {@inheritdoc}
   */
  public function getUrls(): array {
    $path = $this->getPath();
    $cache = $this->cacheBackend->get($path);

    if ($cache === FALSE) {
      $settings = [];

      foreach (static::TYPES as $os) {
        try {
          foreach ($this->fileSystem->scanDirectory("$path/$os", '/^\d+x\d+\.' . APPTILES_IMAGE_EXTENSION . '$/', ['recurse' => FALSE]) as $file) {
            $settings[$os][$file->name] = $file->uri;
          }
        }
        catch (NotRegularDirectoryException $e) {
        }
      }

      $this->cacheBackend->set($path, $settings);
    }
    else {
      $settings = $cache->data;
    }

    foreach (static::TYPES as $os) {
      $settings += [$os => []];
    }

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function isAvailable(): bool {
    return !empty($this->getSetting('allowed_for_admin_theme')) || !$this->adminContext->isAdminRoute();
  }

}
