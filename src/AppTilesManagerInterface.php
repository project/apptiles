<?php

namespace Drupal\apptiles;

/**
 * Application tiles interface.
 *
 * @see \apptiles_page_attachments()
 * @see \apptiles_cache_flush()
 */
interface AppTilesManagerInterface {

  /**
   * The list of operating systems the tiles can be configured for.
   */
  public const TYPES = ['ios', 'android', 'windows'];

  /**
   * Returns the settings.
   *
   * @return array
   *   The settings.
   */
  public function getSettings(): array;

  /**
   * Returns the value of a particular setting.
   *
   * @param string $setting
   *   The name of a setting.
   * @param mixed $default_value
   *   The value to return if a specified setting is undefined.
   *
   * @return mixed
   *   The value of a setting or a default one.
   */
  public function getSetting(string $setting, mixed $default_value = NULL): mixed;

  /**
   * Returns the path to directory with tiles.
   *
   * @return string
   *   The path to directory with tiles.
   */
  public function getPath(): string;

  /**
   * Returns the relative paths to existing tiles.
   *
   * @return string[][]
   *   The arrays, grouped by OS name and dimension.
   *
   * @code
   * [
   *   'ios' => [
   *     '57x57' => 'path/to/image1.png',
   *     '60x60' => 'path/to/image2.png',
   *   ],
   *   'android' => [
   *     '96x96' => 'path/to/image3.png',
   *     '194x194' => 'path/to/image4.png',
   *   ],
   *   'windows' => [
   *     70x70' => 'path/to/image5.png',
   *     '310x310' => 'path/to/image6.png',
   *   ],
   * ]
   * @endcode
   */
  public function getUrls(): array;

  /**
   * Returns the state of whether the configuration available on a page.
   *
   * @return bool
   *   The state of a check.
   */
  public function isAvailable(): bool;

}
